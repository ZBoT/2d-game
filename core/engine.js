/* global process */

module.exports =  {
    init : function(io){
        ioSocket = io;
        
        map.setSize(32, 32);
        map.setLayout("0");
        map.setBlock("brick", "0", 2, 2, true);
        map.setLayout("1");
        map.setBlock("brick", "1", 2, 3, false);
        
        io.on("connection", function(socket){
            var player = new Player(socket);
            player.onConnection();
            player.addReceiveHandler("disconnect", player.onDisconnect);
            player.addReceiveHandler("ping", player.onPing);
            player.addReceiveHandler("playerMove", player.onPlayerMove);
            player.addReceiveHandler("playerTestConnection", player.onPlayerTestConnection);
            player.startTestConnection();
            
            players.addPlayer(player);
        });
        
        process.stdin.on('data', function(chunk){
            var value = chunk.toString().replace(/\s+$/, '');
            switch (value){
                case '/getPlayerList':
                    console.log(players.getPlayers());
                break;
                default:
                    console.log("Error");
                break;
            }
        });
    }
};

var ioSocket;

var Player = function(socket){
    this.socket = socket;
    this.id = socket.id;
    this.position = { "x" : 0, "y" : 0};
    this.connected = false;
    var self = this;
    
    this.startTestConnection = function(){
        var testConnectionTimer = setInterval(function(){
            self.socket.emit("playerTestConnection");
            setTimeout(function(){
                if(self.connected === false){
                    clearInterval(testConnectionTimer);
                    
                    disconnect();
                }
            }, 1000);
        }, 1000);
    };
    
    this.onConnection = function(){
        self.socket.emit("loadChunk", map.getMap());
        self.socket.emit("loadPlayers", players.getPlayers(self));
        self.socket.broadcast.emit("playerConnected", {"id" : self.id, "position" : self.position});
        
        console.log(self.socket.id + " connected!");
    };
    
    this.onDisconnect = function(){
        disconnect();
    };
    
    this.onPlayerTestConnection = function(msg){
        if(msg !== self.socket.id){
            self.connected = false;
        }
        else{
            self.connected = true;
        }
    };
    
    this.onPing = function(){
        self.socket.emit("ping");
    };
    
    this.onPlayerMove = function(msg){
        var position = {"x" : self.position.x, "y" : self.position.y};
        
        for(var directionIndex in msg){
            switch (msg[directionIndex]) {
                case 0:
                    position.y -= 1;
                    break;
                case 1:
                    position.x += 1;
                    break;
                case 2:
                    position.y += 1;
                    break;
                case 3:
                    position.x -= 1;
                    break;
                    
            }
        }
        
        if(position.x >= 0 && position.y >= 0 && position.x < map.getSize().width && position.y < map.getSize().height ){
            if(map.getBlock(position.x, position.y) === undefined || !map.getBlock(position.x, position.y).solid){
                if(players.getPlayerByPos(position.x, position.y) === undefined){
                    self.position.x = position.x;
                    self.position.y = position.y;
                    
                    ioSocket.emit("playerMove", {"id" : self.id, "x" : position.x, "y" : position.y});
                    
                    map.getOnCollision(position.x, position.y, self);
                }
            }
        }
    };
    
    this.addReceiveHandler = function(event, handler){
        socket.on(event, handler);
    };
    
    var disconnect = function(player){
        self.socket.broadcast.emit("playerDisconnected", {"id":self.id});
        self.socket.removeAllListeners();
        self.socket.disconnect();
        players.delPlayer(self.id);
        
        console.log(self.socket.id + " disconnected!");
    };
};

var PlayerContainer = function (){
    var players = {};
    
    this.addPlayer = function(player){
        players[player.id] = player;
    };
    
    this.getPlayer = function(key){
        return players[key];
    };
    
    this.getPlayerByPos = function(x, y){
        for(var i in players){
            var player = players[i];
            if(player.position.x === x && player.position.y === y){
                return players[i];
            }
        }
    };
    
    this.delPlayer = function(key){
        delete players[key];
    };
    
    this.getPlayers = function(withOut){
        var result = {};
        
        for (var i in players) {
            if(withOut === undefined ||  withOut.id !== players[i].id){
                result[i] = {"id":players[i].id, "position":players[i].position};
            }
        }
        
        return result;
    };
};

var players = new PlayerContainer();

var MapManager = function(){
    var map = {"chunk" : {}};
    var mapCollision = {"chunk" : [ ]};
    
    this.setLayout = function(name){
        map.chunk[name] = [];
    };
    
    this.setSize = function(width, height){
        map.size = {width : width, height : height};
        mapCollision = {width : width, height : height};
    };
    
    this.setBlock = function(img, layoutName, x, y, solid){
      map.chunk[layoutName].push({"block" : img, "x": x, "y" : y, "solid" : solid});
    };
    
    this.setOnCollision = function(x, y, event){
        mapCollision.chunk.push({"x" : x, "y" : y, "event" : event});
    };
    
    this.getLayout = function(layoutName){
        return map.chunk[layoutName];
    };
    
    this.getSize = function(){
        return map.size;
    };
    
    this.getOnCollision = function(x, y, player){
        for(var i in mapCollision.chunk){
            var block = mapCollision.chunk[i];
            if(block.x === x && block.y === y){
                block.event(player);
            }
        }
    };
    
    this.getBlock = function(x, y){
        for(var layoutIndex in map.chunk){
            var layout = map.chunk[layoutIndex];
            for (var blockindex in layout) {
                var block = layout[blockindex];
                if(block.x === x && block.y === y){
                    var result = block;
                    result.layout = layoutIndex;
                    
                    return result;
                }
            }
        }
        
        return undefined;
    };
    
    this.getMap = function(){
        return map;
    };
};

var map = new MapManager();