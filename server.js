/* global __dirname */

var http = require('http');
var path = require('path');

var socketio = require('socket.io');
var express = require('express');
//var engine = require('./core/engine');

var router = express();
var server = http.createServer(router);
var io = socketio.listen(server);

router.use(express.static(path.resolve(__dirname, 'game')));

//engine.init(io);

server.listen(80, function(){
  console.log('Server started on!');
});