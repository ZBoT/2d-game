/* global Phaser */

var Engine = function () {
    var game = new Phaser.Game(400, 400, Phaser.AUTO, "Test", {preload: preload, create: create, update: update});

    function preload() {
        
    }

    function create() {
        
    }

    function update() {
        
    }
};

var Player = function (x, y, render) {
    this.nickname;
    this.position = {'x': x, 'y': y};
    this.renderObject;

    this.render = render;
};